﻿using TestProject.Models;
using TestProject.Models.Clients.Requests;
using TestProject.Models.Clients.Responces;
using HttpClient = TestProject.Clients.HttpClient;

namespace TestProject.Services
{
    public class ClientsService
    {
        private string hostUrl;
        private HttpClient _httpClient;
        public ClientsService(string hostUrl)
        {
            this.hostUrl = hostUrl;
            _httpClient = new HttpClient();
        }
        public async Task<BaseResponse<ClientResponse>> CreateClient(ClientRequest requestBody)
        {
            return await _httpClient.PostAsync<ClientResponse>(hostUrl, "/client",requestBody);
        }

    }
}
