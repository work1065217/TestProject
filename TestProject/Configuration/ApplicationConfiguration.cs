﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.Configuration
{
    public class ApplicationConfiguration
    {
        private static ApplicationConfiguration _configuration;
        public ServiceHosts serviceHosts { get; set; }
        public static ApplicationConfiguration GetConfiguration()
        {
            string text = File.ReadAllText(@"appsettings.json");
            _configuration ??= JsonConvert.DeserializeObject<ApplicationConfiguration>(text);
            return _configuration;
        }
    }
}
