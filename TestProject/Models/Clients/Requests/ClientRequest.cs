﻿using Newtonsoft.Json;

namespace TestProject.Models.Clients.Requests
{
    public class ClientRequest
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public dynamic id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public dynamic name { get; set; }
    }
}
