﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.Models;

namespace TestProject.Clients
{
    public class HttpClient
    {
        private static StringContent MakeJsonContent<T>(T content)
        {
            var json = JsonConvert.SerializeObject(content);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }
        public async Task<BaseResponse<T>> PostAsync<T>(string hostUrl, string targetURL, object request) where T : class
        {
            BaseResponse<T> responseObject = new BaseResponse<T>();
            string json = string.Empty;
            try
            {
                using (var client = new System.Net.Http.HttpClient())
                {
                    var content = MakeJsonContent(request);
                    client.DefaultRequestHeaders.Add("Connection", "Keep-Alive");
                    client.DefaultRequestHeaders.Add("Accept-Language", CultureInfo.CurrentCulture.Name);

                    var response = await client.PostAsync(hostUrl + targetURL, content);

                    json = await response.Content.ReadAsStringAsync();

                    Console.WriteLine("URL: POST " + hostUrl + targetURL + "\n" + "Body: " + await content.ReadAsStringAsync() + "\n" + "Response:" + json + "\n" + "Time: " + DateTime.UtcNow + "\n");
                    responseObject.Response = JsonConvert.DeserializeObject<T>(json);
                    responseObject.StatusCode = (int)response.StatusCode;
                }
            }
            catch (HttpRequestException exception)
            {
                Console.WriteLine(exception);
            }
            return responseObject;
        }
    }
}
