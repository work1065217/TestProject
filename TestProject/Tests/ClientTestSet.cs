﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestProject.Configuration;
using TestProject.Models.Clients.Requests;
using TestProject.Services;

namespace TestProject.Tests
{
    [TestClass]
    public class ClientTestSet
    {
        private static readonly ApplicationConfiguration _configuration =  ApplicationConfiguration.GetConfiguration();
        private static ClientsService clientService;

        [ClassInitialize]
        public static async Task ClassInitialize(TestContext context)
        {
            clientService = new ClientsService(_configuration.serviceHosts.TestServiceHost);
        }
        [TestMethod]
        [Description("Позитивный кейс \n" +
            "1.POST /client, Ожидаемый результат: 200 код ответа, name в модели ответа соответствует отрпавленному в теле запроса")]
        public async Task Positive_CreateClient()
        {
            var clientRequestBody = new ClientRequest()
            {
                id = 1,
                name = "test"
            };
            var clientRequest = await clientService.CreateClient(clientRequestBody);
            Assert.AreEqual(200, clientRequest.StatusCode,"Ошибка при вызове /client");
            Assert.AreEqual(clientRequestBody.name, clientRequest.Response.name, "Имя клиента не соответствует указанному в теле запроса");
        }
        [TestMethod]
        [Description("Негативный кейс \n" +
            "Отправка запроса без name в теле \n" +
            "1.POST /client, Ожидаемый результат: НЕ 200 код ответа, сервер обработал ошибку")]
        public async Task Negative_CreateClient_withoutNameRequestBody()
        {
            var clientRequestBody = new ClientRequest()
            {
                id = 1
            };
            var clientRequest = await clientService.CreateClient(clientRequestBody);
            Assert.AreNotEqual(200, clientRequest.StatusCode, "Удалось совершить запрос с некоректным телом (without Name), получен 200 код ответа");
        }
        [TestMethod]
        [Description("Негативный кейс \n" +
            "Отправка запроса без id в теле \n" +
            "1.POST /client, Ожидаемый результат: НЕ 200 код ответа, сервер обработал ошибку \n")]
        public async Task Negative_CreateClient_withoutIdRequestBody()
        {
            var clientRequestBody = new ClientRequest()
            {
                name= "test"
            };
            var clientRequest = await clientService.CreateClient(clientRequestBody);
            Assert.AreNotEqual(200, clientRequest.StatusCode, "Удалось совершить запрос с некоректным телом (without Id), получен 200 код ответа");
        }
        [TestMethod]
        [Description("Негативный кейс \n" +
            "Отправка запроса с неверным типом данных id - string вместо int \n" +
            "1.POST /client, Ожидаемый результат: НЕ 200 код ответа, сервер обработал ошибку \n")]
        public async Task Negative_CreateClient_invalidDataFormat_Id()
        {
            var clientRequestBody = new ClientRequest()
            {
                id = "1",
                name = "test"
            };
            var clientRequest = await clientService.CreateClient(clientRequestBody);
            Assert.AreNotEqual(200, clientRequest.StatusCode, "Удалось совершить запрос с некоректным телом (Id - string), получен 200 код ответа");
        }
        [TestMethod]
        [Description("Негативный кейс \n" +
            "Отправка запроса с неверным типом данных name - int вместо string \n" +
            "1.POST /client, Ожидаемый результат: НЕ 200 код ответа, сервер обработал ошибку \n")]
        public async Task Negative_CreateClient_invalidDataFormat_Name()
        {
            var clientRequestBody = new ClientRequest()
            {
                id = 1,
                name = 1
            };
            var clientRequest = await clientService.CreateClient(clientRequestBody);
            Assert.AreNotEqual(200, clientRequest.StatusCode, "Удалось совершить запрос с некоректным телом (Name - int), получен 200 код ответа");
        }
    }
}
